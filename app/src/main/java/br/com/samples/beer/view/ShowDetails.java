package br.com.samples.beer.view;

import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import br.com.samples.beer.R;
import br.com.samples.beer.model.Beer;

public class ShowDetails extends AppCompatActivity {

    static final String TAG = "SHOW_BEER_ACTIVITY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        Intent intent = getIntent();
        Beer myBeer = (Beer) intent.getSerializableExtra(MainActivity.BEER_KEY);

        TextView textView = findViewById(R.id.tv_name);
        textView.setText(myBeer.getNameBeer());

        Log.d(TAG, myBeer.toString());

    }
}
