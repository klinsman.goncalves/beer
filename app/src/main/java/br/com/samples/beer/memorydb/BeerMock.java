package br.com.samples.beer.memorydb;

import java.util.ArrayList;
import java.util.List;

import br.com.samples.beer.model.Beer;

public class BeerMock {
    List<Beer> beers;
    static BeerMock instance = null;

    private BeerMock(){
        beers = new ArrayList<Beer>();
    }

    public static BeerMock getInstance(){
        if(instance == null){
            instance =  new BeerMock();
        }
        return instance;
    }

    public void save(Beer beer){
        beers.add(beer);
    }

    public List<Beer> listAll(){
        return beers;
    }
}
