package br.com.samples.beer.model;

import java.io.Serializable;

public class Beer implements Serializable {

    private String nameBeer;
    private String descriptionBeer;
    private Integer teorAlcolico;
    private Boolean lowPrice;
    private Boolean nacional;
    private Boolean online;

    public Beer(){

    }

    public Beer(String nameBeer, String descriptionBeer, Integer teorAlcolico, Boolean lowPrice, Boolean nacional, Boolean online) {
        this.nameBeer = nameBeer;
        this.descriptionBeer = descriptionBeer;
        this.teorAlcolico = teorAlcolico;
        this.lowPrice = lowPrice;
        this.nacional = nacional;
        this.online = online;
    }

    public String getNameBeer() {
        return nameBeer;
    }

    public void setNameBeer(String nameBeer) {
        this.nameBeer = nameBeer;
    }

    public String getDescriptionBeer() {
        return descriptionBeer;
    }

    public void setDescriptionBeer(String descriptionBeer) {
        this.descriptionBeer = descriptionBeer;
    }

    public Integer getTeorAlcolico() {
        return teorAlcolico;
    }

    public void setTeorAlcolico(Integer teorAlcolico) {
        this.teorAlcolico = teorAlcolico;
    }

    public Boolean getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(Boolean lowPrice) {
        this.lowPrice = lowPrice;
    }

    public Boolean getNacional() {
        return nacional;
    }

    public void setNacional(Boolean nacional) {
        this.nacional = nacional;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "nameBeer='" + nameBeer + '\'' +
                ", descriptionBeer='" + descriptionBeer + '\'' +
                ", teorAlcolico=" + teorAlcolico +
                ", lowPrice=" + lowPrice +
                ", nacional=" + nacional +
                ", online=" + online +
                '}';
    }
}
