package br.com.samples.beer.view;

import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.List;

import br.com.samples.beer.R;
import br.com.samples.beer.memorydb.BeerMock;
import br.com.samples.beer.model.Beer;

public class MainActivity extends AppCompatActivity {

    EditText edBeerName;
    EditText edBeerDescription;
    CheckBox cbLowPrice;
    CheckBox cbNational;
    CheckBox cbOnline;
    RadioGroup rgAlcohol;
    Button btCadastrar;
    Button btViewList;


    public static final String BEER_KEY = "BEER_KEY";

    String TAG = "ACTIVITY_BEER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edBeerName =  findViewById(R.id.ed_name);
        edBeerDescription =  findViewById(R.id.ed_description);

        cbLowPrice = findViewById(R.id.cb_low_price);
        cbNational = findViewById(R.id.cb_national);
        cbOnline = findViewById(R.id.cb_online);
        rgAlcohol = findViewById(R.id.radioGroup);
        btCadastrar = findViewById(R.id.bt_cadastrar);
        btViewList = findViewById(R.id.bt_listar);

        btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBeer();
            }
        });

        btViewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onclick");
                viewList();
            }
        });
    }

    private void saveBeer() {
        Beer beer =  new Beer();

        beer.setNameBeer(edBeerName.getText().toString());
        beer.setDescriptionBeer(edBeerDescription.getText().toString());
        beer.setLowPrice(cbLowPrice.isChecked());
        beer.setNacional(cbNational.isChecked());
        beer.setOnline(cbOnline.isChecked());

        rgAlcohol.getCheckedRadioButtonId();

        switch (rgAlcohol.getCheckedRadioButtonId()){
            case R.id.rb_alcohol:
                beer.setTeorAlcolico(0);
                break;
            case R.id.rb_non_alcohol:
                beer.setTeorAlcolico(1);
            default:
                beer.setTeorAlcolico(-1);
        }

        Intent intent = new Intent(this, ShowDetails.class);
        intent.putExtra(BEER_KEY, beer);
        startActivity(intent);


        BeerMock.getInstance().save(beer);

        saveNotification();
        //Log.d(TAG, beer.toString());

    }

    static final String NOTIFICATION_CHANNEL =  "BEER";
    private void saveNotification(){
        Log.d(TAG, "save notify");
        NotificationCompat.Builder saveNotification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                .setContentText("Bebida adicionada com sucesso!")
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Cadastrado");

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(1, saveNotification.build());
    }

    public void viewList(){
        Log.d(TAG, "Listing: ----- ");
        List<Beer> beerList = BeerMock.getInstance().listAll();

        for (Beer beer: beerList) {
            Log.d(TAG, beer.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
